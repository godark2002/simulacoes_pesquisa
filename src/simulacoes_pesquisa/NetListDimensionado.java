/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacoes_pesquisa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Vitor
 */
public class NetListDimensionado implements Comparable<NetListDimensionado> {
    
    private List<Cenario> listaCenarios;
    
    private Double somaMaioresValoresRazao;
    private String dimensionamentoParametros;
    private int posPiorCaso;
    private boolean propagacaoValida;
    
    private Double consPiorAvrgHL;
    private Double consPiorAvrgLH;
    
    //totConsHlLH = consPiorAvrgHL + consPiorAvrgLH
    private Double totConsHlLH;

    private Double maiorPHLSv;
    private Double maiorPLHSv;
    
    //Média de todos NSD dos Cenários
    private Double averageNsdHL;
    private Double averageNsdLH;
    
    //Média de todos NED dos Cenários
    private Double averageNedHL;
    private Double averageNedLH;     
    
    //Métrica Ordenação
    private Double totVuln;
    //Posicao para ordenação
    private Double posOrigList2Ord;
    
    
    public NetListDimensionado() 
    {
        this.propagacaoValida = true;
        this.listaCenarios = new ArrayList<>();
        this.totVuln = 0.0;
    }
    public void calcularCasoCritico()
    {
        int posTempPiorCasoTemp=0;
        Double valorTemp = Double.MIN_VALUE;
        try
        {
            for(int i=0; i<this.listaCenarios.size(); i++)
            {
                this.listaCenarios.get(i).clcMaiorDiferenca();

                if(this.listaCenarios.get(i).getTotalDiferenca()>valorTemp)
                {
                    posTempPiorCasoTemp = i;
                    valorTemp = this.listaCenarios.get(i).getTotalDiferenca();
                }
            }
        }
        catch(Exception e)
        {
            this.setPropagacaoValida(false);
        }
        this.posPiorCaso = posTempPiorCasoTemp;
        this.somaMaioresValoresRazao = this.listaCenarios.get(posTempPiorCasoTemp).getTotalDifRazaoMenMaior();        
    }
    public double clcTotalDimensionamento(Map<String, Integer> mapClustersWeights)
    {
        Double total = 0d;
        String[] vetorDimens=this.dimensionamentoParametros.replaceAll("\r","").replaceAll("'", "").split(" ");
        
        for(String stringDimens : vetorDimens)
        {
            if(!stringDimens.isEmpty())
            {
                String linha[] = stringDimens.split("=");
                total+=Double.parseDouble(linha[1])*mapClustersWeights.get(linha[0]);
            }
        }
        
        return total;
    }
    public double clcTotalDimensionamentoParcial()
    {
        Double total = 0d;
        String[] vetorDimens=this.dimensionamentoParametros.split("'");
        
        for(String stringDimens : vetorDimens)
        {
            try            
            {
                if(!stringDimens.contains("="))
                {
                    total+=Double.parseDouble(stringDimens);
                }
            }
            catch(Exception e)
            {
            }
        }
        
        return total;
    }
    public boolean verificarPropagacaoSVMaior(List<Cenario> listaCenarios)
    {
        boolean saidaBool = true;
        
        for(Cenario cenario : listaCenarios)
        {
            for(ElementoSimulacao elementoSimulacao : cenario.getListaElementos())
            {
                try
                {
                    if( (elementoSimulacao.getpLH_S()>elementoSimulacao.getpLH_SV() ) || (elementoSimulacao.getpHL_S()>elementoSimulacao.getpHL_SV()))
                    {
                        saidaBool = false;
                    }
                }
                catch(Exception e)
                {
                    saidaBool = false;
                }
            }
        }
        
        return saidaBool;
    
    }
    public void highSvPropagation(boolean isValidationGate)
    {
        Double maiorPHLSVTemp = Double.MIN_VALUE;
        Double maiorPLHSVTemp = Double.MIN_VALUE;
        for(Cenario cenario : this.listaCenarios)
        {
            //Pega a propagacao critica para HL e LH do SV
            if(isValidationGate)
                cenario.highSvPropagation();
            else
                cenario.highDirectSPropagation();

            if(maiorPHLSVTemp < cenario.getMaiorPHLSv())
            {
                maiorPHLSVTemp = cenario.getMaiorPHLSv();
            }
            if(maiorPLHSVTemp < cenario.getMaiorPLHSv())
            {
                maiorPLHSVTemp = cenario.getMaiorPLHSv();
            }
        }

        this.maiorPHLSv = maiorPHLSVTemp;
        this.maiorPLHSv = maiorPLHSVTemp;
    }
    public void clcTodosAvrgCenConsHlLh()
    {
        Double piorCenConsHL=Double.NEGATIVE_INFINITY;
        Double piorCenConsLH=Double.NEGATIVE_INFINITY;
        Double totCons=piorCenConsHL+piorCenConsLH;
        
        for(Cenario cenario : listaCenarios)
        {
            cenario.clcAvrgConsHlLh();
        }
        
        for(Cenario cenario : listaCenarios)
        {
            if(totCons<cenario.getConsMedioHL()+cenario.getConsMedioLH())
            {
                piorCenConsHL=cenario.getConsMedioHL();
                piorCenConsLH=cenario.getConsMedioLH();
                totCons = piorCenConsHL + piorCenConsLH;
            }
        }
        
        this.consPiorAvrgHL = piorCenConsHL;
        this.consPiorAvrgLH = piorCenConsLH;
        
        this.totConsHlLH = totCons;
        
    }
    public void insCenario(Cenario cenario)
    {
        this.listaCenarios.add(cenario);
    }
    
    public void delCenario(int pos)
    {
        this.listaCenarios.remove(pos);
    }

    public List<Cenario> getListaCenarios() {
        return listaCenarios;
    }

    public void setListaCenarios(List<Cenario> listaCenarios) {
        this.listaCenarios = listaCenarios;
    }

    public Double getSomaRazMenorMaior() {
        return somaMaioresValoresRazao;
    }

    public void setSomaRazMenorMaior(Double somaDasDiferenças) {
        this.somaMaioresValoresRazao = somaDasDiferenças;
    }

    public void clcValCenariosNedNsd()
    {
        if(this.listaCenarios.size()>0)
        {
            this.averageNsdHL = 0.0;
            this.averageNsdLH = 0.0;
            
            this.averageNedHL = 0.0;
            this.averageNedLH = 0.0;
            
            for(Cenario cenario : this.listaCenarios)
            {
                cenario.clcNsdNedMetrics();
                
                this.averageNsdHL += cenario.getNsdHL();
                this.averageNsdLH += cenario.getNsdLH();
                
                this.averageNedHL += cenario.getNedHL();
                this.averageNedLH += cenario.getNedLH();
            }

            this.averageNsdHL = this.averageNsdHL/this.listaCenarios.size();
            this.averageNsdLH = this.averageNsdLH/this.listaCenarios.size();
            
            this.averageNedHL = this.averageNedHL/this.listaCenarios.size();
            this.averageNedLH = this.averageNedLH/this.listaCenarios.size();
        }
    }

    public String getDimensionamentoParametros() {
        return dimensionamentoParametros;
    }

    public void setDimensionamentoParametros(String dimensionamentoParametros) {
        this.dimensionamentoParametros = dimensionamentoParametros;
    }

    public Double getSomaMaioresValores() {
        return somaMaioresValoresRazao;
    }

    public void setSomaMaioresValores(Double somaMaioresValores) {
        this.somaMaioresValoresRazao = somaMaioresValores;
    }

    public int getPosPiorCaso() {
        return posPiorCaso;
    }

    public void setPosPiorCaso(int posPiorCaso) {
        this.posPiorCaso = posPiorCaso;
    }

    public Double getSomaMaioresValoresRazao() {
        return somaMaioresValoresRazao;
    }

    public void setSomaMaioresValoresRazao(Double somaMaioresValoresRazao) {
        this.somaMaioresValoresRazao = somaMaioresValoresRazao;
    }

    public Double getMaiorPHLSv() {
        return maiorPHLSv;
    }

    public void setMaiorPHLSv(Double maiorPHLSv) {
        this.maiorPHLSv = maiorPHLSv;
    }

    public Double getMaiorPLHSv() {
        return maiorPLHSv;
    }

    public void setMaiorPLHSv(Double maiorPLHSv) {
        this.maiorPLHSv = maiorPLHSv;
    }

    public Double getAverageNsdHL() {
        return averageNsdHL;
    }

    public void setAverageNsdHL(Double averageNsdHL) {
        this.averageNsdHL = averageNsdHL;
    }

    public Double getAverageNsdLH() {
        return averageNsdLH;
    }

    public void setAverageNsdLH(Double averageNsdLH) {
        this.averageNsdLH = averageNsdLH;
    }

    public Double getAverageNedHL() {
        return averageNedHL;
    }

    public void setAverageNedHL(Double averageNedHL) {
        this.averageNedHL = averageNedHL;
    }

    public Double getAverageNedLH() {
        return averageNedLH;
    }

    public void setAverageNedLH(Double averageNedLH) {
        this.averageNedLH = averageNedLH;
    }

    public Double getTotVuln() {
        return totVuln;
    }

    public void setTotVuln(Double valueOrd) {
        this.totVuln = valueOrd;
    }

    public Double getPosOrigList2Ord() {
        return posOrigList2Ord;
    }

    public void setPosOrigList2Ord(Double posOrigList2Ord) {
        this.posOrigList2Ord = posOrigList2Ord;
    }

    public boolean isPropagacaoValida() {
        return propagacaoValida;
    }

    public void setPropagacaoValida(boolean isPropagacaoValida) {
        this.propagacaoValida = isPropagacaoValida;
    }
    
    public Double getConsPiorAvrgHL() {
        return consPiorAvrgHL;
    }

    public void setConsPiorAvrgHL(Double consPiorAvrgHL) {
        this.consPiorAvrgHL = consPiorAvrgHL;
    }

    public Double getConsPiorAvrgLH() {
        return consPiorAvrgLH;
    }

    public void setConsPiorAvrgLH(Double consPiorAvrgLH) {
        this.consPiorAvrgLH = consPiorAvrgLH;
    }

    public Double getTotConsHlLH() {
        return totConsHlLH;
    }

    public void setTotConsHlLH(Double totConsHlLH) {
        this.totConsHlLH = totConsHlLH;
    }
    
    @Override
    public int compareTo(NetListDimensionado o) {
        return (Double.compare(this.totVuln, o.getTotVuln()));
    }
    
}
