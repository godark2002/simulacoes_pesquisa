/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacoes_pesquisa;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Vitor
 */
public class Main 
{
    public static void main(String[] args) throws IOException
    //Main() throws UnsupportedEncodingException
    {
                
        /***********CONFIGURAÇãO***********/
        String sistemaOperac = System.getProperty("os.name").toString();
        
        /***********CALCULAR width_total*******/
        double wMin = 120e-9d;
        
        //Determina qual resultado esperado, True = Ordenar Pelos Melhores resultados; False = Ordenar pela menor diferença entre Fases
        boolean isBetterVuln = true;
        boolean isValidationGate = true;
        //Margem SV maior que S1/S0; Exs: 1.1 => 10%; 1.5 => 50%; 2.5 => 150% 
        float mkSvSlowerS1=1.001f;
        
        int numbFloatPrintBits = 3;
        InterpretarSimulacao interpretarSimulacao;
        /***********CONFIGURAÇãO***********/
        
        List<String> listaNomesMeasures;
        if(sistemaOperac.toLowerCase().equals("linux"))
            if(isValidationGate)
                listaNomesMeasures= new ArrayList<>(Arrays.asList(          "consumo_HL",
                                                                            "consumo_LH",
                                                                            "pHL_S",
                                                                            "pHL_SV",
                                                                            "pLH_S",
                                                                            "pLH_SV"                            
                                                                        ));
            else
               listaNomesMeasures= new ArrayList<>(Arrays.asList(          "consumo_HL",
                                                                            "consumo_LH",
                                                                            "pHL_S",
                                                                            "pLH_S"                    
                                                                        )); 
        else
            if(isValidationGate)
                listaNomesMeasures= new ArrayList<>(Arrays.asList(          "pLH_S",
                                                                            "pHL_S",
                                                                            "pHL_SV",
                                                                            "pLH_SV",
                                                                            "consumo_LH",
                                                                            "consumo_HL"                   
                                                                        ));
            else
               listaNomesMeasures= new ArrayList<>(Arrays.asList(           "pLH_S",
                                                                            "pHL_S",
                                                                            "consumo_LH",
                                                                            "consumo_HL"                            
                                                                        )); 
                                
        String path = System.getProperty("java.class.path");
        String filePathEscrita = URLDecoder.decode(path, "UTF-8");
        
        //REMOVE O .JAR do caminho
        if(sistemaOperac.toLowerCase().equals("linux"))
            filePathEscrita = filePathEscrita.substring(0, path.lastIndexOf("/"));
        else
            filePathEscrita = filePathEscrita.substring(0, path.lastIndexOf("\\"));
        
        //teste
        /**************VARIADOS*************/
//         filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/CMOS/xor/";
         
        /**************NAND BSTTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand/";
        /**************NOR BSTTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor/";

        /**************NAND BSTTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand_MVT/";
        /**************NOR BSTTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor_MVT/";

        /**************NAND BSTTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand_MVT_invSupNormal/";
        /**************NOR BSTTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor_MVT_invSupNormal/";

        /**************NAND BSTTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand_SV_2inv/";
        /**************NOR BSTTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor_SV_2inv/";

        /**************NAND BSTTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand_SV_4inv/";
        /**************NOR BSTTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor_SV_4inv/";        
        
        /**************NAND STTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand/";
        /**************NOR STTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor/";
        /**************XOR STTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor/";

        /**************NAND STTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_MVT/";
        /**************NOR STTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor_MVT/";
        /**************XOR STTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor_MVT/";
        
        /**************NAND STTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_MVT_invSupNormal/";
        /**************NOR STTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor_MVT_invSupNormal/";
        /**************XOR STTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor_MVT_invSupNormal/";

        /**************NAND STTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_SV_2inv/";
        /**************NOR STTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor_SV_2inv/";
        /**************XOR STTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor_SV_2inv/";

        /**************NAND STTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_SV_4inv/";
        /**************NOR STTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor_SV_4inv/";
        /**************XOR STTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor_SV_4inv/";

        /**************NAND DPPL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/DPPL/nand/";;
        /**************NOR DPPL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/DPPL/nor/";
        /**************XOR DPPL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/DPPL/xor/";
        
        /**************NAND PCSL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/PCSL/nand/";
        /**************NOR PCSL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/PCSL/nor/";
        /**************XOR PCSL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/PCSL/xor/";
        
        /**************NAND WDDL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/WDDL/nand/";
        /**************NOR WDDL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/WDDL/nor/";
        /**************XOR WDDL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/WDDL/xor/";;

        /**************NAND SABL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/SABL/nand/";
        /**************NOR SABL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/SABL/nor/";
        
        /**************ISTTL_c/ Or*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/ISTTL_nandPorOr/nand/";
        
        /**************NAND STTL TESTE_SUPPLY_LATCHES*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/provar_latch/nand/";

        /**************NAND STTL_MVT TESTE_SUPPLY_LATCHES*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/provar_problema_latch/nand_MVT/";

        /**************NAND STTL ST*************/
        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_ST/";


        //teste
        if(sistemaOperac.toLowerCase().equals("linux"))
        {
            String filePathLeituraEntrada   = filePathEscrita + "/entrada.sp";              

            interpretarSimulacao = new InterpretarSimulacao(filePathEscrita+"/Spices", "configuracao.txt", listaNomesMeasures, isValidationGate, mkSvSlowerS1, wMin);
        }
        else
        {
            String filePathLeituraEntrada   = filePathEscrita + "/entrada.sp";              

            interpretarSimulacao = new InterpretarSimulacao(filePathEscrita+"/Spices", "configuracao.txt", listaNomesMeasures, isValidationGate, mkSvSlowerS1, wMin);
        }
        //interpretarSimulacao.analSimOldMetrics(filePathEscrita);
        
        interpretarSimulacao.analSimNsdNed(filePathEscrita, numbFloatPrintBits, isBetterVuln, isValidationGate);
        
        
        /*
        //TESTE -- SABER SE HÁ Consumo positivo
            IO io = new IO();
            
            for(int i=0; i<interpretarSimulacao.getListaConteudoArquivos().size(); i++)
            {
                String file = interpretarSimulacao.getListaConteudoArquivos().get(i);
                
                String[] temp = file.split("consumoCaso1_LH");
                String conteudo = temp[1].split("min_p1_LH")[0];
                
                
                
//                if((conteudo.split("-").length)<3)
//                {
//                    io.escreverArquivo(filePathEscrita+"\\problema_consumoPositivo.txt", "posicao problema "+i);
//                }               
            }
            
        //TESTE
        */
    }
}
