/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacoes_pesquisa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Vitor
 */
public class TodosDimensionamentos 
{   
    List<Double> lstMelhoresCen;
    List<NetListDimensionado> listaNetLists;
    List<String> listaNomesMeasures;
    
    public TodosDimensionamentos(List<String> listaNomesMeasures) 
    {        
        this.listaNetLists = new ArrayList<>();
        this.lstMelhoresCen = new ArrayList<>();
        this.listaNomesMeasures = new ArrayList<>();
        this.listaNomesMeasures.addAll(listaNomesMeasures);
    }
    public List<NetListDimensionado> clcOrdenarMelhoresCasos(boolean isNsdNedParam)
    {
        List<NetListDimensionado> listaOrdenadoMelhoresCasosTemp = new ArrayList<>();
        
        clcPioresCasosCenarios();
        bscLstMelhoresCen();
        Collections.sort(this.lstMelhoresCen);
        
        for(int i=0; i<this.lstMelhoresCen.size();i++)
        {
            for(NetListDimensionado netListDimensionado:this.listaNetLists)
            {
                if(netListDimensionado.getSomaRazMenorMaior()==lstMelhoresCen.get(i))
                {
                    listaOrdenadoMelhoresCasosTemp.add(netListDimensionado);
                }
            }
        }
        
        return listaOrdenadoMelhoresCasosTemp;
    }
    private void clcPioresCasosCenarios()
    {
        for(NetListDimensionado netListDimensionado : this.listaNetLists)
        {
            netListDimensionado.calcularCasoCritico();
        }        
    }
    public void clcTodosCons()
    {
        for(int i=0; i<listaNetLists.size(); i++)
        {
            NetListDimensionado netListDimensionado = listaNetLists.get(i);
                        
            netListDimensionado.clcTodosAvrgCenConsHlLh();
        }
    }
    public void bscLstMelhoresCen()
    {
        this.lstMelhoresCen.clear();
        
        for(NetListDimensionado netListDimensionado:this.listaNetLists)
        {
            if(netListDimensionado.getSomaRazMenorMaior()!=null)
                if(!this.lstMelhoresCen.contains(netListDimensionado.getSomaRazMenorMaior()))
                {
                    this.lstMelhoresCen.add(netListDimensionado.getSomaRazMenorMaior());
                }
        }
    }            
    public void insTodosElementos(List<ElementoSimulacao> listaElementos, int nArcos, int nCenariosSim)
    {
        List<NetListDimensionado> listaCenarios = new ArrayList<>();
        
        Cenario cenario = new Cenario(this.listaNomesMeasures);
        NetListDimensionado grupoCenarios = new NetListDimensionado();
        
        int totalArcosPorDimensionamento = nArcos * nCenariosSim;
        
        for(int i=0; i<listaElementos.size(); i++)
        {
            cenario.insElemsSim(listaElementos.get(i));
            
            if((i+1)%nArcos==0)
            {
                grupoCenarios.insCenario(cenario);
                
                if((i+1)%totalArcosPorDimensionamento==0)
                {
                    listaCenarios.add(grupoCenarios);
                    grupoCenarios = new NetListDimensionado();
                }
                
                cenario = new Cenario(this.listaNomesMeasures);
            }
        }
        this.listaNetLists.addAll(listaCenarios);
    }    
    
    public void clcValCenariosNetlistsNedNsd()
    {
        for(int i=0; i<this.listaNetLists.size(); i++)
        {
            NetListDimensionado netListDimensionado= this.listaNetLists.get(i);
                        
            netListDimensionado.clcValCenariosNedNsd();
        }
    }
    
    public List<NetListDimensionado> getListaNetLists() {
        return listaNetLists;
    }

    public void setListaNetLists(List<NetListDimensionado> listaNetLists) {
        this.listaNetLists = listaNetLists;
    }

    public List<Double> getLstMelhoresCen() {
        return lstMelhoresCen;
    }

    public void setLstMelhoresCen(List<Double> lstMelhoresCen) {
        this.lstMelhoresCen = lstMelhoresCen;
    }
    
}
