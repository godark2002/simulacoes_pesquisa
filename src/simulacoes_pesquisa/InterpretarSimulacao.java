/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacoes_pesquisa;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/**
 *
 * @author Vitor
 */
public class InterpretarSimulacao 
{
    private TodosDimensionamentos analisadorNetLists;
    private List<NetListDimensionado> lstNetListDimensionadosOrdenados;
    private Map<String, Integer> hmClusterWeight;
    private int nArcos;
    private int nArquivos;
    private int nCenariosSim;
    private String prefixoArq;
    private IOSimula ioSimula;
    private List<String> listaConteudoArquivos;
    private List<String> listaNomesMeasures;
    
    public InterpretarSimulacao(String caminhoPasta, String nomeArqConfig, List<String> listaNomesMeasures, boolean isGateValidation
            , float mrgSvSlowerS1, double wMin) 
    {
        this.hmClusterWeight = new HashMap<>();
        this.listaNomesMeasures = new ArrayList<>();
        this.listaConteudoArquivos = new ArrayList<>();
        this.lstNetListDimensionadosOrdenados = new ArrayList<>();
                
        this.listaNomesMeasures.addAll(listaNomesMeasures);
        
        this.analisadorNetLists = new TodosDimensionamentos(this.listaNomesMeasures);        
        
        if(System.getProperty("os.name").toString().toLowerCase().equals("linux"))
        {
            this.ioSimula = new IOSimula(caminhoPasta+"/"+nomeArqConfig);
            //Ler arquivo entrada.sp para determinar o peso dos clusters
            this.hmClusterWeight.putAll(loadClustersWeights(caminhoPasta.substring(0, caminhoPasta.lastIndexOf("/"))+"/entrada.sp"));
            //Adiciona os parametros de configuracao
            adcElementosLinux();
            //Ler todos os arquivos simulados .log
            this.listaConteudoArquivos.addAll(lerArquivosSimuladosLinux(caminhoPasta)); 
            //Insere todos os dados relevantes dos elementos, de acordo com os números de cenários e dimensionamentos
            this.analisadorNetLists.insTodosElementos(interpArqsSimSpectre(listaConteudoArquivos, isGateValidation, mrgSvSlowerS1), this.nArcos, this.nCenariosSim); 
        
        }
        else{
            this.ioSimula = new IOSimula(caminhoPasta+"\\"+nomeArqConfig);
            //Ler arquivo de configuração e setar os valores da classe
            this.hmClusterWeight.putAll(loadClustersWeights(caminhoPasta.substring(0, caminhoPasta.lastIndexOf("\\"))+"\\entrada.sp"));
            adcElementosWindows();
            //Ler todos os arquivos simulados .log
            this.listaConteudoArquivos.addAll(lerArquivosSimuladosWindows(caminhoPasta)); 
            //Insere todos os dados relevantes dos elementos, de acordo com os números de cenários e dimensionamentos
            this.analisadorNetLists.insTodosElementos(interpArqsSimTanner(listaConteudoArquivos, isGateValidation, mrgSvSlowerS1), this.nArcos, this.nCenariosSim); 
        
        }
        
        if(isGateValidation)
            computarSinalValidade(this.analisadorNetLists, isGateValidation);
        
        /****************TESTAR VALIDADE DOS NETLISTS*****************/
        /*
        List<NetListDimensionado> listTeste = new ArrayList<>();
        boolean isValid=true;
        for(NetListDimensionado netList : this.analisadorNetLists.getListaNetLists())
        {
            for(Cenario cenario :netList.getListaCenarios()) 
            {
                if(!isValid)
                    break;
                for(ElementoSimulacao elemento : cenario.getListaElementos())
                    if(!elemento.isDadoValido())
                    {
                        isValid = false;
                        break;
                    }
            }
            
            if(isValid)
                listTeste.add(netList);
            
            isValid = true;
        }
        */
        /****************        FIM TESTE       *****************/
        
        //Busca parâmetros de configuração
        dimensionarParam(caminhoPasta, prefixoArq);     
        //Calcula todos a média dos consumos HL e LH
        this.analisadorNetLists.clcTodosCons();
    }
    
    public void analSimOldMetrics(String caminhoPasta, boolean isGateVal)
    {           
        //Responsável por calcular as vulnerabilidades e os melhores casos
        this.lstNetListDimensionadosOrdenados.addAll(analisadorNetLists.clcOrdenarMelhoresCasos(false));  
        
        //Responsável por verificar os dados válidos,
        //  Dados não válidos: 1. propSv maior que propS; 2. Simulação/Analisador apresentou algum erro no resultado
        computarSinalValidade(analisadorNetLists, isGateVal);
        
        saidaResultados_old(this.lstNetListDimensionadosOrdenados, caminhoPasta+"\\resultado_old_metrics.txt", isGateVal);
    }
    
    public void analSimNsdNed(String caminhoPasta, int numFloatDig, Boolean isBetterVuln, boolean isGateVal)
    {           
        //Responsável por calcular todos os NSDs e NEDs
        this.analisadorNetLists.clcValCenariosNetlistsNedNsd();
                
        //Responsável por calcular as vulnerabilidades e os melhores casos
        //this.lstNetListDimensionadosOrdenados.addAll(analisadorNetLists.clcOrdenarMelhoresCasos(true));  
        
        //Responsável por verificar os dados válidos,
        //  Dados não válidos: 1. propSv maior que propS; 2. Simulação/Analisador apresentou algum erro no resultado
        computarSinalValidade(analisadorNetLists,isGateVal);
        
        //Responsável por calcular os pesos que será utilizado na ordenação
        clcWeightsSort(isBetterVuln);
        
        if(System.getProperty("os.name").toString().toLowerCase().equals("linux"))
            saidaResultados_lit(this.analisadorNetLists.getListaNetLists(), numFloatDig
                            ,caminhoPasta+"/resultado_lit_metrics"+"_"+(isBetterVuln?"bestVulns":"Equaly")+".txt"
                            , isBetterVuln, isGateVal);
        else
            saidaResultados_lit(this.analisadorNetLists.getListaNetLists(), numFloatDig
                            ,caminhoPasta+"\\resultado_lit_metrics"+"_"+(isBetterVuln?"bestVulns":"Equaly")+".txt"
                            , isBetterVuln, isGateVal);
        //saidaResultados_old(this.lstNetListDimensionadosOrdenados, caminhoPasta+"\\resultado_lit_metrics.txt");
    }
    
    private void clcWeightsSort(Boolean isBetterVuln)
    {
        for(NetListDimensionado netListDimensionado : this.analisadorNetLists.getListaNetLists())
        {
            netListDimensionado.setTotVuln(isBetterVuln
                ?(netListDimensionado.getAverageNedHL()+netListDimensionado.getAverageNsdHL())
                +(netListDimensionado.getAverageNedLH()+netListDimensionado.getAverageNsdLH())
                :Math.abs(
                        (netListDimensionado.getAverageNedHL()+netListDimensionado.getAverageNsdHL())
                        -(netListDimensionado.getAverageNedLH()+netListDimensionado.getAverageNsdLH()))
                );
        }        
    }
    
    private Map<String, Integer> loadClustersWeights(String path)
    {
        Map<String, Integer> hmTemp = new HashMap<>();
        
        IO io = new IO();
        String cntFile = io.lerArquivo(path);
        
        cntFile = cntFile.split(".INICIOEND")[1].split(".END")[0];
        
        cntFile = cntFile.replaceAll("'", "").replaceAll(".param ", "").replaceAll("\r", "");
        
        for(String linha : cntFile.split("\n"))
        {
            if(linha.isEmpty())
                continue;
            
            String cntLinha[] = linha.split("=");
            
            hmTemp.put(cntLinha[0], Integer.parseInt(cntLinha[1]));            
        }
        
        return hmTemp;
    }
    
    //Dados não válidos: 1. propSv maior que propS; 2. Simulação/Analisador apresentou algum erro no resultado
    private void computarSinalValidade(TodosDimensionamentos todosDimensionamentos, boolean isGateValidation)
    {
        for(NetListDimensionado netListDimensionado : todosDimensionamentos.getListaNetLists())
        {
            if(isGateValidation)
                netListDimensionado.setPropagacaoValida(netListDimensionado.verificarPropagacaoSVMaior(netListDimensionado.getListaCenarios()));
            
            if(netListDimensionado.isPropagacaoValida())
            {
                boolean isBreak=false;
                        
                for(Cenario cenario:netListDimensionado.getListaCenarios())
                {
                    for(ElementoSimulacao elementoSimulacao:cenario.getListaElementos())
                    {
                        if(!elementoSimulacao.isDadoValido())
                        {
                            netListDimensionado.setPropagacaoValida(false);
                            isBreak=true;
                            break;
                        }
                    }
                    if(isBreak) 
                        break;
                }
            }
        }        
    }
    private void saidaResultados_lit(List<NetListDimensionado> lstNetListDimensionadosNaoOrdenados, int numFloatDig, String destinoResultado, Boolean isBetterVuln, boolean isValidationGate)
    {
        String saida="";
        
        PriorityQueue<NetListDimensionado> pqSortedVuln = new PriorityQueue<>();
        int i=0;
        
        //SortNetLists sortNetLists = new SortNetLists();        
        //listSorted.addAll(sortNetLists.sort(lstNetListDimensionadosNaoOrdenados, isBetterVuln));
        
        for(double j=0; j<lstNetListDimensionadosNaoOrdenados.size(); j++)
        {            
            NetListDimensionado netDimen = lstNetListDimensionadosNaoOrdenados.get((int)j);
            
            if(!netDimen.isPropagacaoValida())
                continue;
            
            try{
                //Buscar o pior atraso
                netDimen.highSvPropagation(isValidationGate);

            }
            catch(Exception e)
            {
                System.out.println("");
            }
            
            netDimen.setPosOrigList2Ord((j*netDimen.getListaCenarios().size()
                *netDimen.getListaCenarios().get(0).getListaElementos().size()));
            
            pqSortedVuln.add(netDimen);
        }
        
        while(pqSortedVuln.size()>0)
        {
            NetListDimensionado netListDimensionado = pqSortedVuln.poll();
            
            if(!netListDimensionado.isPropagacaoValida())
                continue;   
                        
            saida += "Dimensionamento "+i;
            
            saida += "\nsoma: "+netListDimensionado.clcTotalDimensionamento(this.hmClusterWeight);
            
            saida += "\nsomaParcial: "+netListDimensionado.clcTotalDimensionamentoParcial();
            
            saida += "\nposicao arq_"
                    +netListDimensionado.getPosOrigList2Ord().toString().substring(0, netListDimensionado.getPosOrigList2Ord().toString().indexOf("."))
                    +".sp";
            
            saida += "\nVuln Tot = "+netListDimensionado.getTotVuln();
            
            saida += "\nVuln HL = "+parse2LimtFloatNumb( netListDimensionado.getAverageNsdHL()+netListDimensionado.getAverageNedHL(), numFloatDig);
            saida += "\nVuln LH = "+parse2LimtFloatNumb( netListDimensionado.getAverageNsdLH()+netListDimensionado.getAverageNedLH(), numFloatDig);
            
            saida += "\nConsumo HL= "+parse2LimtFloatNumb(netListDimensionado.getConsPiorAvrgHL(), numFloatDig);
            saida += "\tConsumo LH= "+parse2LimtFloatNumb(netListDimensionado.getConsPiorAvrgLH(), numFloatDig);
            saida += "\nConsumo Total= "+parse2LimtFloatNumb((netListDimensionado.getConsPiorAvrgHL()+netListDimensionado.getConsPiorAvrgLH()), numFloatDig);
            
            saida += "\nMedia NSD HL= "+parse2LimtFloatNumb(netListDimensionado.getAverageNsdHL(), numFloatDig);
            saida += "\tMedia NSD LH="+parse2LimtFloatNumb(netListDimensionado.getAverageNsdLH(), numFloatDig);
            
            saida += "\nMedia NED HL= "+parse2LimtFloatNumb(netListDimensionado.getAverageNedHL(), numFloatDig);
            
            saida += "\tMedia NED LH="+parse2LimtFloatNumb(netListDimensionado.getAverageNedLH(), numFloatDig);
            
            saida += "\nWrosts->\nHL= "+netListDimensionado.getMaiorPHLSv();
            saida += "\nLH= "+netListDimensionado.getMaiorPLHSv();   
            
            saida+="\n"+netListDimensionado.getDimensionamentoParametros();
            
            saida += "\n";
            
            i++;
        }      
        
        ioSimula.getIo().escreverArquivo(destinoResultado, saida);
        
    }
    private String parse2LimtFloatNumb(Double value, int numCharAfterDot)
    {
        String output = "";
        int posTemp ;
        
        posTemp = value.toString().indexOf(".");
        
        if(value.toString().contains("E"))
            output = value.toString().substring(0, posTemp)+"."
                    +value.toString().substring(posTemp+1, numCharAfterDot+posTemp+1)+"E"+value.toString().split("E")[1];
        else
            try
            {
                output = value.toString().substring(0, posTemp)+"."
                        +value.toString().substring(posTemp+1, numCharAfterDot+posTemp+1);
            }
            catch(Exception e)
            {
                output = value.toString();
            }
        return output;    
    }
    
    private void saidaResultados_old(List<NetListDimensionado> lstNetListDimensionadosOrdenados, String destinoResultado, boolean isValidationGate)
    {
        String saida="";
        
        for(NetListDimensionado netListDimensionado : lstNetListDimensionadosOrdenados)
        {
            String difRaz = "";
            String absolute = "";        
            
            //Conflito com a nova versão
            //netListDimensionado.clcTotalDimensionamento();
            
            //Pega a propagacao critica para HL e LH do SV
            netListDimensionado.highSvPropagation(isValidationGate);
            
            if(netListDimensionado.isPropagacaoValida())
            {            
                for(String nomeMeasure : listaNomesMeasures)
                {                
                    //Temps para printar os valores
                            
                    String totDifTemp = netListDimensionado.getListaCenarios().get(netListDimensionado.getPosPiorCaso()).
                                            getHashDiferencaMeas().get(nomeMeasure).toString();
                    String difRazTemp = netListDimensionado.getListaCenarios().get(netListDimensionado.getPosPiorCaso()).
                                            getHashDiferencaMeasRazaoMenorPMaior().get(nomeMeasure).toString();
                    
                    try
                    {
                        if(!totDifTemp.contains("E"))
                        {
                            if(totDifTemp.equals("0.0")){}
                            else if(totDifTemp.length()<7)
                            {
                                totDifTemp = totDifTemp.substring(0,totDifTemp.length()-1);
                            }
                            else
                            {
                                totDifTemp = totDifTemp.substring(0,7);
                            }
                        }
                        else
                        {
                            totDifTemp = totDifTemp.substring(0, totDifTemp.indexOf(".")+1) + totDifTemp.substring(totDifTemp.indexOf(".")+1, totDifTemp.indexOf(".")+4) 
                                + totDifTemp.substring(totDifTemp.indexOf("E"), totDifTemp.length());
                        }
                        if(!difRazTemp.contains("E"))
                        {
                            difRazTemp = difRazTemp.substring(0, difRazTemp.indexOf("."))+"."+difRazTemp.substring(difRazTemp.indexOf(".")+1, 
                                difRazTemp.indexOf(".")+6);
                        }
                        else
                        {
                            difRazTemp = difRazTemp.substring(0, difRazTemp.indexOf(".")+1) + difRazTemp.substring(difRazTemp.indexOf(".")+1, difRazTemp.indexOf(".")+4) 
                                + difRazTemp.substring(difRazTemp.indexOf("E"), difRazTemp.length());
                        }
                        
                    }
                    catch(Exception e)
                    {
                        if(totDifTemp.indexOf("E")==-1)
                        {
                            totDifTemp = "0.0";
                            difRazTemp = "0.0";
                        }
                        else
                        {
                            totDifTemp = totDifTemp.substring(0, totDifTemp.indexOf(".")+1) + totDifTemp.substring(totDifTemp.indexOf(".")+1, totDifTemp.indexOf(".")+totDifTemp.indexOf("E")) 
                            + totDifTemp.substring(totDifTemp.indexOf("E"), totDifTemp.length());
                            try{
                                difRazTemp = difRazTemp.substring(0, difRazTemp.indexOf("."))+"."+difRazTemp.substring(difRazTemp.indexOf(".")+1, 
                                                                                                            difRazTemp.indexOf(".")+6);
                            }
                            catch(Exception f)
                            {
                                System.err.println(f);
                            }
                        }
                    }
                   
                    
                    totDifTemp = totDifTemp.replaceAll(" ", "");
                    difRazTemp = difRazTemp.replaceAll(" ", "");
                    
                    difRaz += nomeMeasure+": "+difRazTemp+"\t";
                    absolute += nomeMeasure+": "+totDifTemp.toString()+"\t";
                }
                
                //Conflito com a nova versão
                //saida +="Soma: "+netListDimensionado.getSomaValoresDimensionamento()+"\t\t";
                saida += netListDimensionado.getDimensionamentoParametros()+"\n";
                saida += "Soma RzMen/Maior-1: "+netListDimensionado.getSomaRazMenorMaior()+"\t\tcrit_pHL: "+netListDimensionado.getMaiorPHLSv()
                                +"\t\tcrit_pLH: "+netListDimensionado.getMaiorPLHSv()+"\n";

                saida += "razao_MenorVlr/MaiorVlr: "+difRaz;
                saida += "\nabsoluto: "+absolute;
                
                saida += "\n";
                
                saida += "\n\n";
            }
        }
        
        ioSimula.getIo().escreverArquivo(destinoResultado, saida);
    }
    
    
    private void dimensionarParam(String caminhoPasta, String prefixArq)
    {
        List<String> listaTemp = new ArrayList<>();
        
        if(System.getProperty("os.name").toString().toLowerCase().equals("linux"))
            listaTemp.addAll(bscrDimensionamentosParam(caminhoPasta+"/"));
        else
            listaTemp.addAll(bscrDimensionamentosParam(caminhoPasta+"\\"));
        
        for(int i=0; i<listaTemp.size(); i++)
        {
            this.analisadorNetLists.getListaNetLists().get(i).setDimensionamentoParametros(listaTemp.get(i));    
        }
    }
    private List<String> bscrDimensionamentosParam(String caminhoPasta)
    {
        List<String> listaTemp = new ArrayList<>();
        
        for(int i=0; i<this.nArquivos/(this.nArcos*this.nCenariosSim); i++)
        {
            String arquivo = ioSimula.getIo().lerArquivo(caminhoPasta+prefixoArq+"_"+(i*(this.nArcos*this.nCenariosSim)+".sp" ));
            
            arquivo = arquivo.split(".INICIOEND")[1];
            
            arquivo = arquivo.replaceAll(".END", "");            
            arquivo = arquivo.replaceAll("\n", ""); 
            arquivo = arquivo.replaceAll(".param", "");
            
            listaTemp.add(arquivo);
         }        
        return listaTemp;
    }    
    //Popula os atributos ElementoSimulacao com os dados simulados
    private List<ElementoSimulacao> interpArqsSimTanner(List<String> listaConteudoArquivos, boolean isGateVal, float mrgSvSlowerS1)
    {    
        List<ElementoSimulacao> listaElemTemp = new ArrayList<>();
        List<Double> listaResultadoDimens;
        List<String> listaConteudoArquivosTemp = new ArrayList<>();
        
        //Remover de todos os arquivos simulados
        listaConteudoArquivosTemp.addAll(remConteudoNRelevanteSimTanner(listaConteudoArquivos));
                
        for(int i=0; i<listaConteudoArquivosTemp.size(); i++)
        {
            String conteudoArq = listaConteudoArquivosTemp.get(i);
            listaResultadoDimens = new ArrayList<>();
            ElementoSimulacao elementoSimulacao;
            
            if(i==72)
                System.out.println("");
            
            listaResultadoDimens.addAll(bscNumRelevantesSimulacao(conteudoArq));
            try{
                if(!conteudoArq.contains("FatalError"))
                {
                    if(isGateVal)
                    {
                        elementoSimulacao=new ElementoSimulacao(
                                listaResultadoDimens.get(0), listaResultadoDimens.get(1), listaResultadoDimens.get(2), listaResultadoDimens.get(3),
                                listaResultadoDimens.get(4), listaResultadoDimens.get(5), listaResultadoDimens.get(6), listaResultadoDimens.get(7),
                                listaResultadoDimens.get(8), listaResultadoDimens.get(9)
                            );  
                    
                        if(elementoSimulacao.getpHL_S()*mrgSvSlowerS1>elementoSimulacao.getpHL_SV()||elementoSimulacao.getpLH_S()*mrgSvSlowerS1>elementoSimulacao.getpLH_SV())
                                elementoSimulacao.setDadoValido(false);
                            
                    }
                    else
                        elementoSimulacao=new ElementoSimulacao(
                                listaResultadoDimens.get(0), listaResultadoDimens.get(1), listaResultadoDimens.get(2), listaResultadoDimens.get(3),
                                listaResultadoDimens.get(4), listaResultadoDimens.get(5), listaResultadoDimens.get(6), listaResultadoDimens.get(7)
                            );
                    
                    
                    if(listaResultadoDimens.toString().contains("null") || listaConteudoArquivos.get(i).contains("Warning :"))
                    {
                        elementoSimulacao.setDadoValido(false);
                    }
                }
                else
                {
                    elementoSimulacao = new ElementoSimulacao(Double.NaN, Double.NaN, Double.NaN, Double.MIN_NORMAL, Double.MAX_VALUE, Double.NaN, Double.MAX_VALUE, Double.NaN);
                    elementoSimulacao.setDadoValido(false);
                }
            }
            catch(Exception e)
            {
                elementoSimulacao=new ElementoSimulacao();
            }
            listaElemTemp.add(elementoSimulacao);            
        }
        
        return listaElemTemp;
    }
    //Popula os atributos ElementoSimulacao com os dados simulados
    private List<ElementoSimulacao> interpArqsSimSpectre(List<String> listaConteudoArquivos, boolean isGateVal, float mrgSvSlowerS1)
    {    
        List<ElementoSimulacao> listaElemTemp = new ArrayList<>();
        List<Double> listaResultadoDimens;
        List<String> listaConteudoArquivosTemp = new ArrayList<>();
        
        if(System.getProperty("os.name").toString().toLowerCase().equals("linux"))
            //Remover de todos os arquivos simulados
            listaConteudoArquivosTemp.addAll(remConteudoNRelevanteSimLinux(listaConteudoArquivos));
        else
            //Remover de todos os arquivos simulados
            listaConteudoArquivosTemp.addAll(remConteudoNRelevanteSimTanner(listaConteudoArquivos));
            
        for(int i=0; i<listaConteudoArquivosTemp.size(); i++)
        {
            String conteudoArq = listaConteudoArquivosTemp.get(i);
            listaResultadoDimens = new ArrayList<>();
            ElementoSimulacao elementoSimulacao;
                        
            try{
                if(!conteudoArq.contains("NaN"))
                {
                    HashMap<String, Double> hmResultados = new HashMap<>();

                    for(String paramResult : conteudoArq.split("\n"))
                    {
                        for(String measure : this.listaNomesMeasures) //pHL_SV
                        {
                            if(paramResult.split("=")[0].replaceAll("\r","").equals(measure.toLowerCase()))
                            {
                                hmResultados.put(measure.replaceAll("\r",""), Math.abs(Double.parseDouble(paramResult.split("=")[1].replace("\t", ""))));
                                break;    
                            }                                
                        }
                    } 
                    elementoSimulacao = new ElementoSimulacao(hmResultados);
                    
                    if(isGateVal)
                        if(elementoSimulacao.getpHL_S()*mrgSvSlowerS1>elementoSimulacao.getpHL_SV()||elementoSimulacao.getpLH_S()*mrgSvSlowerS1>elementoSimulacao.getpLH_SV())
                            elementoSimulacao.setDadoValido(false);
                    
                }else
                {
                    elementoSimulacao = new ElementoSimulacao(Double.NaN, Double.NaN, Double.NaN, Double.MIN_NORMAL, Double.MAX_VALUE, Double.NaN, Double.MAX_VALUE, Double.NaN);
                    elementoSimulacao.setDadoValido(false);
                }
            }
            catch(Exception e)
            {
                elementoSimulacao = new ElementoSimulacao(Double.NaN, Double.NaN, Double.NaN, Double.MIN_NORMAL, Double.MAX_VALUE, Double.NaN, Double.MAX_VALUE, Double.NaN);
                elementoSimulacao.setDadoValido(false);
            }
            
            listaElemTemp.add(elementoSimulacao); 
        }
        
        return listaElemTemp;
    }
    //Pegar apenas valores relevantes da simulacao
    private List<Double> bscNumRelevantesSimulacao(String conteudoArquivosRelevante)
    {
        List<Double> listaSaida = new ArrayList<>();
        
        String[] linhaArquivo=conteudoArquivosRelevante.split("\n");
        
        for(int i=0; i<linhaArquivo.length; i++)
        {
            try
            {
                String vlrRelev = linhaArquivo[i].split("=")[1].replaceAll("\t","");
                
                listaSaida.add(Double.parseDouble(vlrRelev));
            }
            catch(Exception e)
            {
                listaSaida.add(null);
            }
        }
        return listaSaida;
    }
    //Remove conteudo não relevante do arquivo simulado
    private List<String> remConteudoNRelevanteSimTanner(List<String> listaConteudoArquivos)
    {
        List<String> listaConteudoArquivosTemp = new ArrayList<>();
                
        //TESTE
        int cont=0;
        
        
        for(int i=0; i<listaConteudoArquivos.size(); i++)
        {
            String[] temp;
            String conteudoArq = listaConteudoArquivos.get(i);
            
            String formatado="";
            
            temp = conteudoArq.split("Measurement result summary");
            
            //temp = temp[1].split("Parsing");
            
            //TESTE
            try
            {
                temp = temp[1].split("Parsing");
                //System.out.println(cont);
            }
            catch(Exception e)
            {
                System.out.print("");
            }
            cont++;
            //TESTE
            temp[0] = temp[0].replaceAll(" ", "");
            try{
                temp[0] = temp[0].substring(2, temp[0].length()-2);
            }
            catch(Exception e)
            {
                temp[0] = temp[0].substring(2, temp[0].length()-2);
            }
            listaConteudoArquivosTemp.add(temp[0].replaceAll("\r", ""));
            
        }
        
        return listaConteudoArquivosTemp;
    }
    //Remove conteudo não relevante do arquivo simulado
    private List<String> remConteudoNRelevanteSimLinux(List<String> listaConteudoArquivos)
    {
        List<String> listaConteudoArquivosTemp = new ArrayList<>();
                
        
        for(int i=0; i<listaConteudoArquivos.size(); i++)
        {
            String temp;
            String conteudoArq = listaConteudoArquivos.get(i);
            
            String formatado="";
            
            temp = conteudoArq.split("tran\n")[1];
            
            String aux;
            do
            {
                aux = temp;
                temp = temp.replaceAll(" ", "");
            }while(!aux.equals(temp));
            
            String[] aux2=temp.split("\n");
            temp="";
            for (String linha : aux2) 
            {
                if(!linha.equals(""))
                    temp += linha+"\n";
            }
            
            listaConteudoArquivosTemp.add(temp.replaceAll("\r", ""));
            
        }
        
        return listaConteudoArquivosTemp;
    }
    //TESTE para saber se todos os consumos são negativos-- Remove conteudo não relevante do arquivo simulado --
    private List<String> TESTEremConteudoNRelevanteSimTESTE(List<String> listaConteudoArquivos)
    {
        List<String> listaConteudoArquivosTemp = new ArrayList<>();
                
        //TESTE
        int cont=0;
        
        
        for(int i=0; i<listaConteudoArquivos.size(); i++)
        {
            String[] temp;
            String conteudoArq = listaConteudoArquivos.get(i);
            
            String formatado="";
            
            temp = conteudoArq.split("Measurement result summary");
            
            //temp = temp[1].split("Parsing");
            
            //TESTE
            try
            {
                temp = temp[1].split("Parsing");
                //System.out.println(cont);
            }
            catch(Exception e)
            {
                System.out.print("");
            }
            cont++;
            //TESTE
            temp[0] = temp[0].replaceAll(" ", "");
            //temp[0] = temp[0].replaceAll("-", "");
            try{
                temp[0] = temp[0].substring(2, temp[0].length()-2);
            }
            catch(Exception e)
            {
                temp[0] = temp[0].substring(2, temp[0].length()-2);
            }
            listaConteudoArquivosTemp.add(temp[0]);
            
        }
        
        return listaConteudoArquivosTemp;
    }
    
    //Remove os simbolos de ordem de grandeza por apenas números
    private Double cvtrNumCSimbPorNum (String valor)
    {
        Double resultado;
        DecimalFormat form;
        String valorTemp;
        
        valor = valor.replaceAll(" ", "");
        char simbolo;
        
        simbolo = valor.charAt(valor.length()-1);
        
        valor = valor.substring(0, valor.length()-1);
        
        resultado = Double.parseDouble(valor);
        
        switch (simbolo)
        {
            case 'm':
                form = new DecimalFormat("0.000000000000000000");
                valorTemp = form.format(resultado*0.001);
                valorTemp = valorTemp.replaceAll(",", ".");
                resultado = Double.parseDouble(valorTemp);
                break;
            case 'u':
                form = new DecimalFormat("0.000000000000000000");
                valorTemp = form.format(resultado*0.000001);
                valorTemp = valorTemp.replaceAll(",", "."); 
                resultado = Double.parseDouble(valorTemp);
                break;
            case 'n':
                form = new DecimalFormat("0.000000000000000000");
                valorTemp = form.format(resultado*0.000000001);
                valorTemp = valorTemp.replaceAll(",", ".");
                resultado = Double.parseDouble(valorTemp);
                break;
            case 'p':
                form = new DecimalFormat("0.000000000000000000");
                valorTemp = form.format(resultado*0.000000000001);
                valorTemp = valorTemp.replaceAll(",", ".");                
                resultado = Double.parseDouble(valorTemp);
                break;
            case 'f':
                form = new DecimalFormat("0.000000000000000000");
                valorTemp = form.format(resultado*0.000000000000001);
                valorTemp = valorTemp.replaceAll(",", ".");                
                resultado = Double.parseDouble(valorTemp);                
                break;
            default:
                resultado = Double.parseDouble(valor+simbolo);
                break;
        }
        
        return resultado;
    }
    //Ler todos os arquivos simulados .log
    private List<String> lerArquivosSimuladosWindows(String caminhoPasta)
    {        
        List<String> listaArqsTemp = new ArrayList<String>();
               
        for(int i=0; i<this.nArquivos; i++)
        {
            listaArqsTemp.add(ioSimula.lerArquivo(caminhoPasta+"\\"+this.prefixoArq+"_"+i+".log"));
        }
        
        return listaArqsTemp;
    }    
    //Ler todos os arquivos simulados .log
    private List<String> lerArquivosSimuladosLinux(String caminhoPasta)
    {        
        List<String> listaArqsTemp = new ArrayList<String>();
        IO io = new IO();
        
        for(int i=0; i<this.nArquivos; i++)
        {
            
            try
            {
                String caminho = caminhoPasta+"/"+this.prefixoArq+"_"+i+".measure";
                listaArqsTemp.add(ioSimula.lerArquivo(caminho));
            }catch(Exception e)
            {
                listaArqsTemp.add("");
            }
        }
        
        return listaArqsTemp;
    } 
    //Ler arquivo de configuração e setar os valores da classe
    public void adcElementosWindows()
    {
        int temp;
        
        temp = Integer.parseInt(ioSimula.getMapConfiguracoes().get("nArcos").substring(0, ioSimula.getMapConfiguracoes().get("nArcos").length()-1));
        this.nArcos = temp;
        temp = Integer.parseInt(ioSimula.getMapConfiguracoes().get("nCenariosSim").substring(0, ioSimula.getMapConfiguracoes().get("nCenariosSim").length()-1));
        this.nCenariosSim = temp;
        temp = Integer.parseInt(ioSimula.getMapConfiguracoes().get("nArquivos").substring(0, ioSimula.getMapConfiguracoes().get("nArquivos").length()-1));
        this.nArquivos = temp;
        this.prefixoArq = ioSimula.getMapConfiguracoes().get("prefixo").substring(0, ioSimula.getMapConfiguracoes().get("prefixo").length()-1);
    }
    //Ler arquivo de configuração e setar os valores da classe
    public void adcElementosLinux()
    {       
        this.nArcos = Integer.parseInt(ioSimula.getMapConfiguracoes().get("nArcos"));
        this.nCenariosSim = Integer.parseInt(ioSimula.getMapConfiguracoes().get("nCenariosSim"));
        this.nArquivos = Integer.parseInt(ioSimula.getMapConfiguracoes().get("nArquivos"));
        this.prefixoArq = ioSimula.getMapConfiguracoes().get("prefixo");
    }    

    public TodosDimensionamentos getAnalisadorNetLists() {
        return analisadorNetLists;
    }

    public void setAnalisadorNetLists(TodosDimensionamentos analisadorNetLists) {
        this.analisadorNetLists = analisadorNetLists;
    }

    public int getnArcos() {
        return nArcos;
    }

    public void setnArcos(int nArcos) {
        this.nArcos = nArcos;
    }

    public int getnArquivos() {
        return nArquivos;
    }

    public void setnArquivos(int nArquivos) {
        this.nArquivos = nArquivos;
    }

    public int getnCenariosSim() {
        return nCenariosSim;
    }

    public void setnCenariosSim(int nCenariosSim) {
        this.nCenariosSim = nCenariosSim;
    }

    public String getPrefixoArq() {
        return prefixoArq;
    }

    public void setPrefixoArq(String prefixoArq) {
        this.prefixoArq = prefixoArq;
    }

    public IOSimula getIoSimula() {
        return ioSimula;
    }

    public void setIoSimula(IOSimula ioSimula) {
        this.ioSimula = ioSimula;
    }

    public List<String> getListaConteudoArquivos() {
        return listaConteudoArquivos;
    }

    public void setListaConteudoArquivos(List<String> listaConteudoArquivos) {
        this.listaConteudoArquivos = listaConteudoArquivos;
    }
    
}
