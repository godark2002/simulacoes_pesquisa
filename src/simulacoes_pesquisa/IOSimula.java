/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacoes_pesquisa;

import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author Vitor
 */
public class IOSimula {
    private Map<String, String> mapConfiguracoes=null;
    private IO io;
    private String arquivo;
    
    public IOSimula(String caminhoArqSim) {
        this.mapConfiguracoes = new HashMap<>();
        this.io =  new IO();
        
        this.arquivo = this.io.lerArquivo(caminhoArqSim);
        
        mapConfiguracoes.putAll(interpArqConfig(arquivo));
        
    }
    
    private Map<String, String> interpArqConfig(String configSimulacao)
    {        
        configSimulacao.replaceAll("\r", "");
        String[] configuracoesTemp = configSimulacao.split("\n");
        
        for(int i=0; i<configuracoesTemp.length; i++)
        {            
            String nomeTemp = configuracoesTemp[i].split("=")[0];
            String valorTemp = configuracoesTemp[i].split("=")[1];
            
            mapConfiguracoes.put(nomeTemp, valorTemp);
        }
        
        return mapConfiguracoes;
    }
    
    public void escreverArquivo(String caminho, String conteudo)
    {
        io.escreverArquivo(caminho, conteudo);
    }
    
    public String lerArquivo(String caminho)
    {
        return io.lerArquivo(caminho);
    }
    
    public Map<String, String> getMapConfiguracoes() {
        return mapConfiguracoes;
    }

    public void setMapConfiguracoes(Map<String, String> mapConfiguracoes) {
        this.mapConfiguracoes = mapConfiguracoes;
    }

    public IO getIo() {
        return io;
    }

    public void setIo(IO io) {
        this.io = io;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }
        
}
