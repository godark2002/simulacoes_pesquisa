/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacoes_pesquisa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Vitor
 */
public class Cenario {
    
    private List<ElementoSimulacao> listaElementos;
    
    private Map<String, Double> hashDiferencaMeas;
    private Map<String, Double> hashDiferencaMeasRazaoMenorPMaior;
   
    private Map<String, Double> hashMenoresValores;
    private Map<String, Double> hashMaioresValores;
    
    private List<String> listNomesMeasures;
    
    private Double totalDiferenca;
    private Double totalDifRazaoMenMaior;
    
    private Double maiorPHLSv;
    private Double maiorPLHSv;
    
    private Double consMedioHL;
    private Double consMedioLH;
    
    //NED = (Max(E)-Min(E))/Max(E)
    private Double nedHL;
    private Double nedLH;
    //NSD = standardDeviation/standardArithmetic
    private Double nsdHL;
    private Double nsdLH;
    //metric = NSD + NED
    private Double metricHL;
    private Double metricLH;
    
    private Double standardDeviationHL;
    private Double averageArithmeticHL;
    
    private Double standardDeviationLH;
    private Double averageArithmeticLH;
    
    
    public Cenario(List<String> listaNomesMeasures) {
        this.hashDiferencaMeas = new HashMap<>();
        this.hashDiferencaMeasRazaoMenorPMaior = new HashMap<>();
        this.hashMaioresValores = new HashMap<>();
        this.hashMenoresValores = new HashMap<>();
        this.listaElementos = new ArrayList<>();   
        this.listNomesMeasures = new ArrayList<>();
        
        
        this.listNomesMeasures.addAll(listaNomesMeasures);
    }
    public void clcNsdNedMetrics()    {
        //CALCULA NED = (Max(E)-Min(E))/Max(E)
        Double maxTempE;
        Double minTempE;
        
        //Calculando NED HL
        maxTempE=getMaxValueE(true);
        minTempE=getMinValueE(true);        
        this.nedHL = (maxTempE-minTempE)/maxTempE;
        
        //Calculando NED LH
        maxTempE=getMaxValueE(false);
        minTempE=getMinValueE(false);        
        this.nedLH = (maxTempE-minTempE)/maxTempE;
        
        //CALCULAR NSD = standardDeviation/standardArithmetic    
        this.averageArithmeticHL = clcAverageArit(true);
        this.averageArithmeticLH = clcAverageArit(false);
        
        this.standardDeviationHL = clcStandardDeviation(this.listaElementos, clcAverageArit(true), true);
        this.standardDeviationLH = clcStandardDeviation(this.listaElementos, clcAverageArit(false), false);
        
        this.nsdHL = this.standardDeviationHL / this.averageArithmeticHL;
        this.nsdLH = this.standardDeviationLH / this.averageArithmeticLH;
        
        //Calcular metrica = NSD + NED
        this.metricHL = this.nsdHL + this.nedHL;
        this.metricLH = this.nsdLH + this.nedLH;
        
    }    
    public void clcSomaDiferenças() {
        Double somaTemp = 0d;
        for(String nome:listNomesMeasures)
        {
            somaTemp += hashDiferencaMeas.get(nome);
        }
        
        this.totalDiferenca = somaTemp;
    }    
    private Double clcStandardDeviation(List<ElementoSimulacao> listElementoSimulacaos, double average, boolean isHLEnergy)    {
        Double standardDeviation=0.0;
        /*
        //TESTAR Fórmula de Desvio Padrão
            //Resultado esperado teste para populcao = 2.0
            //Resultado esperado teste para amostra = 2.19...
            average = 8;               
            Double stndVarTemp=0.0;
            List<Double> listTest = new ArrayList<>(
                    Arrays.asList(
                        10d,
                        8d,
                        10d,
                        8d,
                        8d,
                        4d
                    ));

            for(Double element : listTest)
                stndVarTemp += Math.pow(element-average, 2);

            //Calculo Populacao
            //stndVarTemp = stndVarTemp/(listTest.size());
            
            //Calculo Amostra
            //stndVarTemp = stndVarTemp/(listTest.size()-1);

            stndVarTemp = Math.sqrt(stndVarTemp);
        //*/
        //FIM TESTE
        
        if(listElementoSimulacaos.size()==1)
                standardDeviation=0.0;
        else
        {
            for(ElementoSimulacao element : listElementoSimulacaos)
                standardDeviation += Math.pow((isHLEnergy?element.getConsumo_HL():element.getConsumo_LH())-average, 2);            
            
            standardDeviation = standardDeviation/(listElementoSimulacaos.size());
            
            standardDeviation = Math.sqrt(standardDeviation);
        }
        return standardDeviation;
    }    
    public void clcAvrgConsHlLh()    {
        Double consHL=0d;
        Double consLH=0d;
        
        for(int i=0; i<this.listaElementos.size(); i++)
        {
            consHL+=this.listaElementos.get(i).getConsumo_HL();
            consLH+=this.listaElementos.get(i).getConsumo_LH();
        }
        
        this.consMedioHL = (Double)(consHL/this.listaElementos.size());
        this.consMedioLH = (Double)(consLH/this.listaElementos.size());
    }
    public void clcMaiorDiferenca() {
        this.hashMenoresValores.clear();
        this.hashMaioresValores.clear(); 
        Double totalMeasTemp = 0d;
        Double totalRazTemp = 0d;
        
        insMeasuresHashs();
        
        this.hashMenoresValores.putAll(clcMnrValorTdsListas());
        this.hashMaioresValores.putAll(clcMaiorValorTdsListas());
        
        this.hashDiferencaMeas.put("pHL_S", Math.abs((this.hashMaioresValores.get("pHL_S")-this.hashMenoresValores.get("pHL_S"))));
        this.hashDiferencaMeas.put("pLH_S", Math.abs((this.hashMaioresValores.get("pLH_S")-this.hashMenoresValores.get("pLH_S"))));
        this.hashDiferencaMeas.put("pHL_SV", Math.abs((this.hashMaioresValores.get("pHL_SV")-this.hashMenoresValores.get("pHL_SV"))));
        this.hashDiferencaMeas.put("pLH_SV", Math.abs((this.hashMaioresValores.get("pLH_SV")-this.hashMenoresValores.get("pLH_SV"))));
        this.hashDiferencaMeas.put("consumo_LH", Math.abs((this.hashMaioresValores.get("consumo_LH")-this.hashMenoresValores.get("consumo_LH"))));
        this.hashDiferencaMeas.put("consumo_HL", Math.abs((this.hashMaioresValores.get("consumo_HL")-this.hashMenoresValores.get("consumo_HL"))));
        this.hashDiferencaMeas.put("min_p1_LH", Math.abs((this.hashMaioresValores.get("min_p1_LH")-this.hashMenoresValores.get("min_p1_LH"))));
        this.hashDiferencaMeas.put("max_p1_LH", Math.abs((this.hashMaioresValores.get("max_p1_LH")-this.hashMenoresValores.get("max_p1_LH"))));
        this.hashDiferencaMeas.put("min_p2_HL", Math.abs((this.hashMaioresValores.get("min_p2_HL")-this.hashMenoresValores.get("min_p2_HL"))));
        this.hashDiferencaMeas.put("max_p2_HL", Math.abs((this.hashMaioresValores.get("max_p2_HL")-this.hashMenoresValores.get("max_p2_HL"))));
        
        
        this.hashDiferencaMeasRazaoMenorPMaior.put("pHL_S", Math.abs((Double)((this.hashMenoresValores.get("pHL_S")/this.hashMaioresValores.get("pHL_S")))-1));
        this.hashDiferencaMeasRazaoMenorPMaior.put("pLH_S", Math.abs(this.hashMenoresValores.get("pLH_S")/this.hashMaioresValores.get("pLH_S")-1));
        this.hashDiferencaMeasRazaoMenorPMaior.put("pHL_SV", Math.abs(this.hashMenoresValores.get("pHL_SV")/this.hashMaioresValores.get("pHL_SV")-1));
        this.hashDiferencaMeasRazaoMenorPMaior.put("pLH_SV", Math.abs(this.hashMenoresValores.get("pLH_SV")/this.hashMaioresValores.get("pLH_SV")-1));
        this.hashDiferencaMeasRazaoMenorPMaior.put("consumo_LH", Math.abs(this.hashMenoresValores.get("consumo_LH")/this.hashMaioresValores.get("consumo_LH")-1));
        this.hashDiferencaMeasRazaoMenorPMaior.put("consumo_HL", Math.abs(this.hashMenoresValores.get("consumo_HL")/this.hashMaioresValores.get("consumo_HL")-1));
        this.hashDiferencaMeasRazaoMenorPMaior.put("min_p1_LH", Math.abs(this.hashMenoresValores.get("min_p1_LH")/this.hashMaioresValores.get("min_p1_LH")-1));
        this.hashDiferencaMeasRazaoMenorPMaior.put("max_p1_LH", Math.abs(this.hashMenoresValores.get("max_p1_LH")/this.hashMaioresValores.get("max_p1_LH")-1));
        this.hashDiferencaMeasRazaoMenorPMaior.put("min_p2_HL", Math.abs(this.hashMenoresValores.get("min_p2_HL")/this.hashMaioresValores.get("min_p2_HL")-1));
        this.hashDiferencaMeasRazaoMenorPMaior.put("max_p2_HL", Math.abs(this.hashMenoresValores.get("max_p2_HL")/this.hashMaioresValores.get("max_p2_HL")-1));
        
        for(String nome : this.listNomesMeasures)
        {
            totalMeasTemp += this.hashDiferencaMeas.get(nome);
            totalRazTemp += this.hashDiferencaMeasRazaoMenorPMaior.get(nome);
        }
        
        this.totalDiferenca = Math.abs(totalMeasTemp);
        this.totalDifRazaoMenMaior = Math.abs(totalRazTemp);
    }
    private void insMeasuresHashs() {
        for(String temp : this.listNomesMeasures)
        {
            this.hashMenoresValores.put(temp, Double.MIN_VALUE);
            this.hashMaioresValores.put(temp, Double.MAX_VALUE);
        }
    }
    private Map<String, Double> clcMnrValorTdsListas()    {
        Map<String, Double> hashMapMnrVlrTemp = new HashMap<>();
        
        hashMapMnrVlrTemp.put("pHL_S", Double.MAX_VALUE);
        hashMapMnrVlrTemp.put("pLH_S", Double.MAX_VALUE);
        hashMapMnrVlrTemp.put("pHL_SV", Double.MAX_VALUE);
        hashMapMnrVlrTemp.put("pLH_SV", Double.MAX_VALUE);
        hashMapMnrVlrTemp.put("consumo_LH", Double.MAX_VALUE);
        hashMapMnrVlrTemp.put("consumo_HL", Double.MAX_VALUE);
        hashMapMnrVlrTemp.put("min_p1_LH", Double.MAX_VALUE);
        hashMapMnrVlrTemp.put("max_p1_LH", Double.MAX_VALUE);
        hashMapMnrVlrTemp.put("min_p2_HL", Double.MAX_VALUE);
        hashMapMnrVlrTemp.put("max_p2_HL", Double.MAX_VALUE);
        
        for(ElementoSimulacao elementoSimulacao : this.listaElementos)
        {            
            if(elementoSimulacao.getConsumo_HL()<hashMapMnrVlrTemp.get("consumo_HL"))
            {
                    hashMapMnrVlrTemp.put("consumo_HL", elementoSimulacao.getConsumo_HL());
            }
            if(elementoSimulacao.getConsumo_LH()<hashMapMnrVlrTemp.get("consumo_LH"))
            {
                    hashMapMnrVlrTemp.put("consumo_LH", elementoSimulacao.getConsumo_LH());
            }
            if(elementoSimulacao.getpHL_S()<hashMapMnrVlrTemp.get("pHL_S"))
            {
                    hashMapMnrVlrTemp.put("pHL_S", elementoSimulacao.getpHL_S());
            }
            if(elementoSimulacao.getpHL_SV()<hashMapMnrVlrTemp.get("pHL_SV"))
            {
                    hashMapMnrVlrTemp.put("pHL_SV", elementoSimulacao.getpHL_SV());
            }
            if(elementoSimulacao.getpLH_S()<hashMapMnrVlrTemp.get("pLH_S"))
            {
                    hashMapMnrVlrTemp.put("pLH_S", elementoSimulacao.getpLH_S());
            }
            if(elementoSimulacao.getpLH_SV()<hashMapMnrVlrTemp.get("pLH_SV"))
            {
                    hashMapMnrVlrTemp.put("pLH_SV", elementoSimulacao.getpLH_SV());
            }  
        }
        return hashMapMnrVlrTemp;
    }
   //Ultima etapa, para pegar os maiores valores
    private Map<String, Double> clcMaiorValorTdsListas() {
        Map<String, Double> hashMapMaiorVlrTemp = new HashMap<>();
        
        hashMapMaiorVlrTemp.put("pHL_S", Double.MIN_VALUE);
        hashMapMaiorVlrTemp.put("pLH_S", Double.MIN_VALUE);
        hashMapMaiorVlrTemp.put("pHL_SV", Double.MIN_VALUE);
        hashMapMaiorVlrTemp.put("pLH_SV", Double.MIN_VALUE);
        hashMapMaiorVlrTemp.put("consumo_LH", Double.MIN_VALUE);
        hashMapMaiorVlrTemp.put("consumo_HL", Double.MIN_VALUE);
        hashMapMaiorVlrTemp.put("min_p1_LH", Double.MIN_VALUE);
        hashMapMaiorVlrTemp.put("max_p1_LH", Double.MIN_VALUE);
        hashMapMaiorVlrTemp.put("min_p2_HL", Double.MIN_VALUE);
        hashMapMaiorVlrTemp.put("max_p2_HL", Double.MIN_VALUE);

        
        for(ElementoSimulacao elementoSimulacao : this.listaElementos)
        {            
            if(elementoSimulacao.getConsumo_HL()> hashMapMaiorVlrTemp.get("consumo_HL"))
            {
                    hashMapMaiorVlrTemp.put("consumo_HL", elementoSimulacao.getConsumo_HL());
            }
            if(elementoSimulacao.getConsumo_LH()> hashMapMaiorVlrTemp.get("consumo_LH"))
            {
                    hashMapMaiorVlrTemp.put("consumo_LH", elementoSimulacao.getConsumo_LH());
            }
            if(elementoSimulacao.getpHL_S()> hashMapMaiorVlrTemp.get("pHL_S"))
            {
                    hashMapMaiorVlrTemp.put("pHL_S", elementoSimulacao.getpHL_S());
            }
            if(elementoSimulacao.getpHL_SV()> hashMapMaiorVlrTemp.get("pHL_SV"))
            {
                    hashMapMaiorVlrTemp.put("pHL_SV", elementoSimulacao.getpHL_SV());
            }
            if(elementoSimulacao.getpLH_S()> hashMapMaiorVlrTemp.get("pLH_S"))
            {
                    hashMapMaiorVlrTemp.put("pLH_S", elementoSimulacao.getpLH_S());
            }
            if(elementoSimulacao.getpLH_SV()> hashMapMaiorVlrTemp.get("pLH_SV"))
            {
                    hashMapMaiorVlrTemp.put("pLH_SV", elementoSimulacao.getpLH_SV());
            }
        }
        return hashMapMaiorVlrTemp;
    }    
    private Double clcAverageArit(boolean isHLEnergy)    {
        double avrgArit=0.0;
        
        for(ElementoSimulacao elem : this.listaElementos)
        {
            avrgArit+=isHLEnergy?elem.getConsumo_HL():elem.getConsumo_LH();
        }
        
        return (avrgArit/this.listaElementos.size());
    }    
    private Double getMinValueE(boolean isHLEnergy)    {
        Double minElem;
        
        minElem = isHLEnergy?this.listaElementos.get(0).getConsumo_HL():this.listaElementos.get(0).getConsumo_LH();
        
        for(ElementoSimulacao elem : this.listaElementos)
        {
            if(isHLEnergy)      
            {
                if(minElem > elem.getConsumo_HL())
                    minElem = elem.getConsumo_HL(); 
            }else
                if((minElem > elem.getConsumo_LH()))
                    minElem = elem.getConsumo_LH();
        }
        
        return minElem;
    }
    private Double getMaxValueE(boolean isHLEnergy)    {
        Double minElem;
        
        minElem = isHLEnergy?this.listaElementos.get(0).getConsumo_HL():this.listaElementos.get(0).getConsumo_LH();
        
        for(ElementoSimulacao elem : this.listaElementos)
        {
            if(isHLEnergy)      
            {
                if(minElem < elem.getConsumo_HL())
                    minElem = elem.getConsumo_HL(); 
            }else
                if((minElem < elem.getConsumo_LH()))
                    minElem = elem.getConsumo_LH();
        }
        
        return minElem;
    }
    public void highSvPropagation() {
        Double maiorPHLSVTemp = Double.MIN_VALUE;
        Double maiorPLHSVTemp = Double.MIN_VALUE;
        
        for(ElementoSimulacao elementoSimulacao : this.listaElementos)
        {
            if(maiorPHLSVTemp < elementoSimulacao.getpHL_SV())
            {
                maiorPHLSVTemp = elementoSimulacao.getpHL_SV();
            }
            if(maiorPLHSVTemp < elementoSimulacao.getpLH_SV())
            {
                maiorPLHSVTemp = elementoSimulacao.getpLH_SV();
            }
        }
        this.maiorPHLSv = maiorPHLSVTemp;
        this.maiorPLHSv = maiorPLHSVTemp;
    }
    public void highDirectSPropagation() {
        Double maiorPHLSVTemp = Double.MIN_VALUE;
        Double maiorPLHSVTemp = Double.MIN_VALUE;
        
        for(ElementoSimulacao elementoSimulacao : this.listaElementos)
        {
            if(maiorPHLSVTemp < elementoSimulacao.getpHL_S())
            {
                maiorPHLSVTemp = elementoSimulacao.getpHL_S();
            }
            if(maiorPLHSVTemp < elementoSimulacao.getpLH_S())
            {
                maiorPLHSVTemp = elementoSimulacao.getpLH_S();
            }
        }
        this.maiorPHLSv = maiorPHLSVTemp;
        this.maiorPLHSv = maiorPLHSVTemp;
    }
    public void insElemsSim(ElementoSimulacao listaElementos) {
        this.listaElementos.add(listaElementos);
    }    
    public void delElemSim(int pos) {
        this.listaElementos.remove(pos);
    }
    public List<ElementoSimulacao> getListaElementos() {
        return listaElementos;
    }
    public void setListaElementos(List<ElementoSimulacao> listaElementos) {
        this.listaElementos = listaElementos;
    }
    public Map<String, Double> getHashMaioresDiferencas() {
        return hashDiferencaMeas;
    }
    public void setHashMaioresDiferencas(Map<String, Double> hashMaioresDiferencas) {
        this.hashDiferencaMeas = hashMaioresDiferencas;
    }
    public Map<String, Double> getHashMaioresDiferencasPercent() {
        return hashDiferencaMeasRazaoMenorPMaior;
    }
    public void setHashMaioresDiferencasPercent(Map<String, Double> hashMaioresDiferencasPercent) {
        this.hashDiferencaMeasRazaoMenorPMaior = hashMaioresDiferencasPercent;
    }
    public Map<String, Double> getHashMenoresValores() {
        return hashMenoresValores;
    }
    public void setHashMenoresValores(Map<String, Double> hashMenoresValores) {
        this.hashMenoresValores = hashMenoresValores;
    }
    public Map<String, Double> getHashMaioresValores() {
        return hashMaioresValores;
    }
    public void setHashMaioresValores(Map<String, Double> hashMaioresValores) {
        this.hashMaioresValores = hashMaioresValores;
    }
    public Map<String, Double> getHashDiferencaMeas() {
        return hashDiferencaMeas;
    }
    public void setHashDiferencaMeas(Map<String, Double> hashDiferencaMeas) {
        this.hashDiferencaMeas = hashDiferencaMeas;
    }
    public Map<String, Double> getHashDiferencaMeasRazaoMenorPMaior() {
        return hashDiferencaMeasRazaoMenorPMaior;
    }
    public void setHashDiferencaMeasRazaoMenorPMaior(Map<String, Double> hashDiferencaMeasRazaoMenorPMaior) {
        this.hashDiferencaMeasRazaoMenorPMaior = hashDiferencaMeasRazaoMenorPMaior;
    }
    public List<String> getListNomesMeasures() {
        return listNomesMeasures;
    }
    public void setListNomesMeasures(List<String> listNomesMeasures) {
        this.listNomesMeasures = listNomesMeasures;
    }
    public Double getTotalDiferenca() {
        return totalDiferenca;
    }
    public void setTotalDiferenca(Double totalDiferenca) {
        this.totalDiferenca = totalDiferenca;
    }
    public Double getTotalDifRazaoMenMaior() {
        return totalDifRazaoMenMaior;
    }
    public void setTotalDifRazaoMenMaior(Double totalDifRazaoMenMaior) {
        this.totalDifRazaoMenMaior = totalDifRazaoMenMaior;
    }
    public Double getMaiorPHLSv() {
        return maiorPHLSv;
    }
    public void setMaiorPHLSv(Double maiorPHLSv) {
        this.maiorPHLSv = maiorPHLSv;
    }
    public Double getMaiorPLHSv() {
        return maiorPLHSv;
    }
    public void setMaiorPLHSv(Double maiorPLHSv) {
        this.maiorPLHSv = maiorPLHSv;
    }
    public Double getNedHL() {
        return nedHL;
    }
    public void setNedHL(Double nedHL) {
        this.nedHL = nedHL;
    }
    public Double getNedLH() {
        return nedLH;
    }
    public void setNedLH(Double nedLH) {
        this.nedLH = nedLH;
    }
    public Double getNsdHL() {
        return nsdHL;
    }
    public void setNsdHL(Double nsdHL) {
        this.nsdHL = nsdHL;
    }
    public Double getNsdLH() {
        return nsdLH;
    }
    public void setNsdLH(Double nsdLH) {
        this.nsdLH = nsdLH;
    }
    public Double getMetricHL() {
        return metricHL;
    }
    public void setMetricHL(Double metricHL) {
        this.metricHL = metricHL;
    }
    public Double getMetricLH() {
        return metricLH;
    }
    public void setMetricLH(Double metricLH) {
        this.metricLH = metricLH;
    }
    public Double getStandardDeviationHL() {
        return standardDeviationHL;
    }
    public void setStandardDeviationHL(Double standardDeviationHL) {
        this.standardDeviationHL = standardDeviationHL;
    }
    public Double getAverageArithmeticHL() {
        return averageArithmeticHL;
    }
    public void setAverageArithmeticHL(Double averageArithmeticHL) {
        this.averageArithmeticHL = averageArithmeticHL;
    }
    public Double getStandardDeviationLH() {
        return standardDeviationLH;
    }
    public void setStandardDeviationLH(Double standardDeviationLH) {
        this.standardDeviationLH = standardDeviationLH;
    }
    public Double getAverageArithmeticLH() {
        return averageArithmeticLH;
    }
    public void setAverageArithmeticLH(Double averageArithmeticLH) {
        this.averageArithmeticLH = averageArithmeticLH;
    }    
    public Double getConsMedioHL() {
        return consMedioHL;
    }

    public void setConsMedioHL(Double consMedioHL) {
        this.consMedioHL = consMedioHL;
    }

    public Double getConsMedioLH() {
        return consMedioLH;
    }

    public void setConsMedioLH(Double consMedioLH) {
        this.consMedioLH = consMedioLH;
    }
}
