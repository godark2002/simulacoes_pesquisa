/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package simulacoes_pesquisa;

import java.util.Map;

/**
 *
 * @author Vitor
 */
public class ElementoSimulacao {
    private Double pHL_S;
    private Double pLH_S;
    private Double pHL_SV;
    private Double pLH_SV;
    private Double consumo_LH;
    private Double consumo_HL;
    private boolean dadoValido;

    public ElementoSimulacao() 
    {
        this.pHL_S = Double.MAX_VALUE;
        this.pLH_S = Double.MAX_VALUE;
        this.pHL_SV = Double.MAX_VALUE;
        this.pLH_SV = Double.MAX_VALUE;
        this.consumo_LH = Double.MAX_VALUE;
        this.consumo_HL = Double.MAX_VALUE;
        this.dadoValido = false;
    }    
    
    
    public ElementoSimulacao(Map<String, Double> hmParametros)
    {
        for(String parametro : hmParametros.keySet())
        {
            switch(parametro)
            {
                case "consumo_HL":  this.consumo_HL = hmParametros.get(parametro);
                    break; 
                case "consumo_LH":  this.consumo_LH = hmParametros.get(parametro);
                    break; 
                case "pHL_S":       this.pHL_S = hmParametros.get(parametro);
                    break; 
                case "pLH_S":       this.pLH_S = hmParametros.get(parametro);
                    break; 
                case "pHL_SV":      this.pHL_SV = hmParametros.get(parametro);
                    break; 
                case "pLH_SV":      this.pLH_SV = hmParametros.get(parametro);
                    break; 
                default:
                    break;
            }
        }
        this.setDadoValido(true);
    }
    public ElementoSimulacao(Double pHL_S, Double pLH_S, Double pHL_SV, Double pLH_SV, Double consumo_LH, Double consumo_HL, Double min_p1_LH, Double max_p1_LH, Double min_p2_HL, Double max_p2_HL) {
        this.pHL_S = pHL_S;
        this.pLH_S = pLH_S;
        this.pHL_SV = pHL_SV;
        this.pLH_SV = pLH_SV;
        this.consumo_LH = Math.abs(consumo_LH);
        this.consumo_HL = Math.abs(consumo_HL);
        this.dadoValido = true;
    }
    
    public ElementoSimulacao(Double pHL_S, Double pLH_S, Double consumo_LH, Double consumo_HL, Double min_p1_LH, Double max_p1_LH, Double min_p2_HL, Double max_p2_HL) {
        this.pHL_S = pHL_S;
        this.pLH_S = pLH_S;
        this.consumo_LH = Math.abs(consumo_LH);
        this.consumo_HL = Math.abs(consumo_HL);
        this.dadoValido = true;
    }

    
    
    public Double getpHL_S() {
        return pHL_S;
    }

    public void setpHL_S(Double pHL_S) {
        this.pHL_S = pHL_S;
    }

    public Double getpLH_S() {
        return pLH_S;
    }

    public void setpLH_S(Double pLH_S) {
        this.pLH_S = pLH_S;
    }

    public Double getpHL_SV() {
        return pHL_SV;
    }

    public void setpHL_SV(Double pHL_SV) {
        this.pHL_SV = pHL_SV;
    }

    public Double getpLH_SV() {
        return pLH_SV;
    }

    public void setpLH_SV(Double pLH_SV) {
        this.pLH_SV = pLH_SV;
    }

    public Double getConsumo_LH() {
        return consumo_LH;
    }

    public void setConsumo_LH(Double consumo_LH) {
        this.consumo_LH = consumo_LH;
    }

    public Double getConsumo_HL() {
        return consumo_HL;
    }

    public void setConsumo_HL(Double consumo_HL) {
        this.consumo_HL = consumo_HL;
    }

    public boolean isDadoValido() {
        return dadoValido;
    }

    public void setDadoValido(boolean dadoValido) {
        this.dadoValido = dadoValido;
    }
       
}
